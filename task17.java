import java.nio.file.Paths;
import java.util.Scanner;

public class Problem17 {

    private static final int[] masses = {57, 71, 87, 97, 99, 101, 103, 113, 114, 115, 128, 129, 131, 137, 147, 156, 163, 186};

    private static long getLinearPeptideCount (int mass) {
        long[] counts = new long[mass + 1];
        counts[0] = 1;
        for (int i = 1; i <= mass; i++) {
            for (int m : masses) {
                if (i - m >= 0) {
                    counts[i] += counts[i - m];
                }
            }
        }
        return counts[mass];
    }

    public static void main(String[] args) {
        try (Scanner scan = new Scanner(Paths.get("dataset.txt"))) {
            int m = scan.nextInt();
            System.out.println(getLinearPeptideCount(m));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}