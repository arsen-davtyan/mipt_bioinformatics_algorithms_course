import java.io.IOException;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Scanner;

public class Problem22 {

    private static int[][] down;
    private static int[][] right;
    private static int[][] path;

    private static void getPath(int n, int m) {
        for (int i = 0; i <= n; i++) {
            for (int j = 0; j <= m; j++) {
                if (i == 0 && j == 0) {
                    continue;
                }

                int x = 0;
                int y = 0;

                if (j >= 1) {
                    x = path[i][j - 1] +  right[i][j - 1];
                }
                if (i >= 1) {
                    y = path[i - 1][j] + down[i - 1][j];
                }
                path[i][j] = Math.max(x, y);
            }
        }
    }

    public static void main(String[] args) {
        try (Scanner scan = new Scanner(Paths.get("dataset.txt"))) {
            int n = scan.nextInt();
            int m = scan.nextInt();
            scan.nextLine();

            down = new int[n][m + 1];
            right = new int[n + 1][m];
            path = new int[n + 1][m + 1];

            for (int i = 0; i < n; i++) {
                down[i] = Arrays.stream(scan.nextLine().split(" ")).mapToInt(Integer::parseInt).toArray();
            }
            scan.nextLine();
            for (int i = 0; i < n + 1; i++) {
                right[i] = Arrays.stream(scan.nextLine().split(" ")).mapToInt(Integer::parseInt).toArray();
            }

            getPath(n, m);
            System.out.println(path[n][m]);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}