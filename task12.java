import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class Problem12 {

    private static Map<String, List<String>> createGraph(List<String> patterns) {
        Map<String, List<String>> graph = new HashMap<>();
        for (String pattern : patterns) {
            String prefix = pattern.substring(0, pattern.length() - 1);
            String suffix = pattern.substring(1);
            if (!graph.containsKey(prefix))
                graph.put(prefix, new ArrayList<>());
            if (!graph.containsKey(suffix))
                graph.put(suffix, new ArrayList<>());
        }
        for (String pattern : patterns) {
            String prefix = pattern.substring(0, pattern.length() - 1);
            String suffix = pattern.substring(1);
            graph.get(prefix).add(suffix);
        }
        return graph;
    }

    private static boolean isEdgeInCycle(List<String> cycle, String node, String node_) {
        for (int i = 0; i < cycle.size() - 1; i++) {
            if (cycle.get(i).equals(node) && cycle.get(i + 1).equals(node_))
                return true;
        }
        return false;
    }

    private static String getNextNode(List<String> cycle, Map<String, List<String>> graph, String node) {
        for (String node_ : graph.get(node)) {
            if (!isEdgeInCycle(cycle, node, node_))
                return node_;
        }
        return "";
    }

    private static String getNewStart(List<String> cycle, Map<String, List<String>> graph) {
        for (String node : cycle) {
            String nextNode = getNextNode(cycle, graph, node);
            if (!nextNode.isEmpty())
                return node;
        }
        return "";
    }

    private static List<String> constructNewCycle(List<String> cycle, String newStart) {
        int position = 0;
        for (int i = 0; i < cycle.size(); i++) {
            if (cycle.get(i).equals(newStart))
                position = i;
        }

        List<String> newCycle = new ArrayList<>(cycle.size());
        for (int i = position; i < cycle.size(); i++) {
            newCycle.add(cycle.get(i));
        }
        for (int i = 1; i <= position; i++) {
            newCycle.add(cycle.get(i));
        }

        return newCycle;
    }

    private static String mergeTogether(List<String> patternsInOrder) {
        int patternSize = patternsInOrder.get(0).length();

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(patternsInOrder.get(0).substring(0,  patternSize - 1));
        for (String pattern : patternsInOrder) {
            stringBuilder.append(pattern.charAt(patternSize - 1));
        }

        return stringBuilder.toString();
    }

    private static String getUniversalString(List<String> patterns, int k) {
        Map<String, List<String>> graph = createGraph(patterns);

        List<String> cycle = new ArrayList<>();
        cycle.add(graph.keySet().stream().findFirst().orElse(null));

        while (!getNewStart(cycle, graph).isEmpty()) {
            String newStart = getNewStart(cycle, graph);
            cycle = constructNewCycle(cycle, newStart);

            while (!getNextNode(cycle, graph, cycle.get(cycle.size() - 1)).isEmpty()) {
                cycle.add(getNextNode(cycle, graph, cycle.get(cycle.size() - 1)));
            }
        }

        String result = mergeTogether(cycle);
        return result.substring(0, result.length() - k + 1);
    }

    public static void main(String[] args) {
        try (Scanner scan = new Scanner(Paths.get("dataset.txt"))) {
            int k = Integer.parseInt(scan.nextLine());
            String patternFormat = "%" + k + "s";
            List<String> patterns = new ArrayList<>();
            for (int i = 0; i < Math.pow(2, k); i++) {
                String pattern = String.format(patternFormat, Integer.toBinaryString(i)).replace(' ', '0');
                patterns.add(pattern);
            }

            String universalString = getUniversalString(patterns, k);
            System.out.println(universalString);

        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
    }
}