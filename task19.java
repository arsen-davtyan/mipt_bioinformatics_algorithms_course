package problem19;

import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class Problem19 {
    private static List<Long> getMassesInSpectrum(List<Long> spectrum) {
        List<Long> masses = Arrays.asList(57L, 71L, 87L, 97L, 99L, 101L, 103L, 113L, 114L, 115L, 128L, 129L, 131L, 137L,
                147L, 156L, 163L, 186L);
        return masses.stream().filter(spectrum::contains).collect(Collectors.toList());
    }

    private static List<Peptide> expand(List<Peptide> peptides, List<Long> masses) {
        List<Peptide> newPeptides = new ArrayList<>();
        for (Peptide p : peptides) {
            for (Long m : masses) {
                Peptide p_ = p.copy();
                p_.add(m);
                newPeptides.add(p_);
            }
        }
        return newPeptides;
    }

    private static int getScore(Peptide peptide, List<Long> spectrum) {
        int score = 0;
        for (Long m : peptide.getSpectrum()) {
            if (spectrum.contains(m))
                score++;
        }
        return score;
    }

    private static List<Peptide> cut(List<Peptide> leaderboard, List<Long> spectrum, int N) {
        if (leaderboard.size() == 0) {
            return new ArrayList<>();
        }
        List<Integer> scores = leaderboard.stream().map(peptide -> getScore(peptide, spectrum)).sorted(Collections.reverseOrder()).collect(Collectors.toList());
        int topScore = scores.get(Collections.min(Arrays.asList(N, scores.size() - 1)));
        List<Peptide> newLeaderboard = leaderboard.stream().filter(peptide -> getScore(peptide, spectrum) >= topScore).collect(Collectors.toList());
        return newLeaderboard;
    }

    private static Peptide leaderboardCyclopeptideSequencing(List<Long> spectrum, int N) {
        List<Long> masses = getMassesInSpectrum(spectrum);
        List<Peptide> leaderboard = new ArrayList<>(Arrays.asList(new Peptide()));
        Peptide leader = new Peptide();
        while (leaderboard.size() > 0) {
            leaderboard = expand(leaderboard, masses);
            List<Peptide> newLeaderboard = new ArrayList<>(leaderboard);
            for (Peptide peptide : leaderboard) {
                if (peptide.getMass() == Collections.max(spectrum)) {
                    if (getScore(peptide, spectrum) > getScore(leader, spectrum)) {
                        leader = peptide.copy();
                    }
                } else if (peptide.getMass() > Collections.max(spectrum)) {
                    newLeaderboard.remove(peptide);
                }
            }
            newLeaderboard = cut(newLeaderboard, spectrum, N);
            leaderboard = newLeaderboard;
        }
        return leader;
    }

    public static void main(String[] args) {
        try (Scanner scan = new Scanner(Paths.get("dataset.txt"))) {
            int N = Integer.parseInt(scan.nextLine());
            List<Long> spectrum = new ArrayList<>();
            while (scan.hasNextLong()) {
                spectrum.add(scan.nextLong());
            }
            Collections.sort(spectrum);
            Peptide leader = leaderboardCyclopeptideSequencing(spectrum, N);
            System.out.println(leader);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

class Peptide {
    private List<Long> masses = new ArrayList<>();

    List<Long> getSpectrum() {
        ArrayList<Long> spectrum = new ArrayList<>(Arrays.asList(0L));
        List<Long> mses = new ArrayList<>(this.masses);
        if (!mses.isEmpty())
            mses.remove(mses.size() - 1);
        int n = this.masses.size();

        for (int k = 1; k < n; k++) {
            int idx = (k - 2) % n;
            if (idx < 0)
                idx += n;
            mses.add(this.masses.get(idx));
            for (int i = 0; i < mses.size() - k + 1; i++) {
                long sum = 0;
                for (int j = i; j < i + k; j++)
                    sum += mses.get(j);
                spectrum.add(sum);
            }
        }
        spectrum.add(getMass());
        Collections.sort(spectrum);
        return spectrum;
    }

    long getMass() {
        return masses.stream().reduce(0L, Long::sum);
    }

    void add(long newMass) {
        this.masses.add(newMass);
    }

    Peptide copy() {
        Peptide copyPeptide = new Peptide();
        copyPeptide.masses = new ArrayList<>(this.masses);
        return copyPeptide;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (long mass : this.masses) {
            sb.append(Long.toString(mass));
            sb.append("-");
        }
        sb.deleteCharAt(sb.length() - 1);
        return sb.toString();
    }
}