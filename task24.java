import java.io.IOException;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.stream.IntStream;

public class Problem24 {
    private static final Map<Character, Integer> symbolIdx = new HashMap<>();
    private static final int[][] scoringMatrix = new int[20][20];

    private static final int sigma = 5;

    private static int getScore(char s1, char s2) {
        return scoringMatrix[symbolIdx.get(s1)][symbolIdx.get(s2)];
    }

    private static String getAlignment(String proteinString, String directionString, char direction, int start) {
        StringBuilder alignment = new StringBuilder();
        int idx = start;
        for (char dir : directionString.toCharArray()) {
            if (dir == direction) {
                alignment.append('-');
            } else {
                alignment.append(proteinString.charAt(idx));
                idx += 1;
            }
        }
        return alignment.toString();
    }

    public static void main(String[] args) {
        char[] symbols = {'A', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'K', 'L', 'M', 'N', 'P', 'Q', 'R', 'S', 'T', 'V', 'W', 'Y'};
        IntStream.range(0, 20).forEach(idx -> symbolIdx.put(symbols[idx], idx));

        try {
            Scanner scan = new Scanner(Paths.get("PAM250.txt"));
            for (int i = 0; i < 20; i++) {
                scoringMatrix[i] = Arrays.stream(scan.nextLine().split("\\s+")).mapToInt(Integer::parseInt).toArray();
            }
            scan.close();

            scan = new Scanner(Paths.get("dataset.txt"));
            String strI = scan.nextLine();
            String strJ = scan.nextLine();
            int n = strI.length();
            int m = strJ.length();

            int[][] path = new int[n + 1][m + 1];
            char[][] direction = new char[n + 1][m + 1];

            int endI = 0;
            int endJ = 0;

            for (int i = 0; i < n + 1; i++) {
                for (int j = 0; j < m + 1; j++) {
                    if (i == 0 && j == 0)
                        continue;

                    int left = -Integer.MAX_VALUE;
                    int up = -Integer.MAX_VALUE;
                    int diag = -Integer.MAX_VALUE;

                    if (i >= 1 && j >= 1)
                        diag = path[i - 1][j - 1] + getScore(strI.charAt(i - 1), strJ.charAt(j - 1));
                    if (j >= 1)
                        left = path[i][j - 1] - sigma;
                    if (i >= 1)
                        up = path[i - 1][j] - sigma;

                    if (left >= up && left >= diag && left >= 0) {
                        path[i][j] = left;
                        direction[i][j] = 'l';
                    } else if (up >= left && up >= diag && up >= 0) {
                        path[i][j] = up;
                        direction[i][j] = 'u';
                    } else if (diag >= left && diag >= up && diag >= 0) {
                        path[i][j] = diag;
                        direction[i][j] = 'd';
                    } else {
                        path[i][j] = 0;
                        direction[i][j] = '0';
                    }

                    if (i == n && j == m) {
                        int max = -Integer.MAX_VALUE;
                        int endK = 0;
                        int endL = 0;

                        for (int k = 0; k < n + 1; k++) {
                            for (int l = 0; l < m + 1; l++) {
                                if (path[k][l] > max) {
                                    max = path[k][l];
                                    endK = k;
                                    endL = l;
                                }
                            }
                        }

                        if (max > left && max > up && max > diag) {
                            endI = endK;
                            endJ = endL;
                            path[i][j] = max;
                        }
                    }
                }
            }

            int startI = 0;
            int startJ = 0;

            StringBuilder directionStringBuilder = new StringBuilder();
            int i = endI;
            int j = endJ;

            while (i != 0 || j != 0) {
                if (direction[i][j] == 'l') {
                    j--;
                    directionStringBuilder.append('r');
                } else if (direction[i][j] == 'u') {
                    i--;
                    directionStringBuilder.append('d');
                } else if (direction[i][j] == 'd') {
                    i--;
                    j--;
                    directionStringBuilder.append('D');
                } else if (direction[i][j] == '0') {
                    startI = i;
                    startJ = j;
                    i = 0;
                    j = 0;
                }
            }

            String directionString = directionStringBuilder.reverse().toString();

            int score = path[n][m];
            String alignmentI = getAlignment(strI, directionString, 'r', startI);
            String alignmentJ = getAlignment(strJ, directionString, 'd', startJ);

            System.out.println(score);
            System.out.println(alignmentI);
            System.out.println(alignmentJ);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}