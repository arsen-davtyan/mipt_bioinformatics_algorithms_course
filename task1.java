import java.util.Scanner;
import java.util.ArrayList;
import java.util.HashMap;
import java.nio.file.Paths;

public class Problem1 {

    private static HashMap<String, ArrayList<Integer>> getPatternIdx(String genome, int k) {
        HashMap<String, ArrayList<Integer>> patternIdx = new HashMap<>();

        for (int i = 0; i <= genome.length() - k; i++) {
            String pattern = genome.substring(i, i + k);
            if (!patternIdx.containsKey(pattern)) {
                patternIdx.put(pattern, new ArrayList<>());
            }
            patternIdx.get(pattern).add(i);
        }

        return patternIdx;
    }

    private static boolean checkClump(ArrayList<Integer> entryIdx, int k, int L, int t) {
        if (entryIdx.size() < t)
            return false;

        for (int i = 0; i <= entryIdx.size() - t; i++) {
            if (entryIdx.get(i + t - 1) - entryIdx.get(i) + k <= L) {
                return true;
            }
        }

        return false;
    }

    private static ArrayList<String> getPatternsFormingClumps(HashMap<String, ArrayList<Integer>> patternIdx, int k, int L, int t) {
        ArrayList<String> patternsFormingClumps = new ArrayList<>();
        for (String pattern : patternIdx.keySet()) {
            if (checkClump(patternIdx.get(pattern), k, L, t)) {
                patternsFormingClumps.add(pattern);
            }
        }
        return patternsFormingClumps;
    }

    public static void main(String[] args) {
        try (Scanner scan = new Scanner(Paths.get("dataset.txt"))) {
            String genome = scan.nextLine();
            int k = scan.nextInt();
            int L = scan.nextInt();
            int t = scan.nextInt();

            HashMap<String, ArrayList<Integer>> patternIdx = getPatternIdx(genome, k);
            ArrayList<String> patternsFormingClumps = getPatternsFormingClumps(patternIdx, k, L, t);

            for (int i = 0; i < patternsFormingClumps.size(); i++) {
                System.out.print(patternsFormingClumps.get(i));
                if (i != patternsFormingClumps.size() - 1);
                    System.out.print(" ");
            }

        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
    }
}