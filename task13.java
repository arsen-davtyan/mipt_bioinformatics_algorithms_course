import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class Problem13 {

    private static Map<String, List<String>> createGraph(List<String> pairedReads) {
        Map<String, List<String>> graph = new HashMap<>();

        for (String read : pairedReads) {
            String[] pair = read.split("\\|");
            String prefix = pair[0].substring(0, pair[0].length() - 1) + "|"
                    + pair[1].substring(0, pair[1].length() - 1);
            String suffix = pair[0].substring(1) + "|" + pair[1].substring(1);
            if (!graph.containsKey(prefix))
                graph.put(prefix, new ArrayList<>());
            if (!graph.containsKey(suffix))
                graph.put(suffix, new ArrayList<>());
        }
        for (String read : pairedReads) {
            String[] pair = read.split("\\|");
            String prefix = pair[0].substring(0, pair[0].length() - 1) + "|"
                    + pair[1].substring(0, pair[1].length() - 1);
            String suffix = pair[0].substring(1) + "|" + pair[1].substring(1);
            graph.get(prefix).add(suffix);
        }
        return graph;
    }

    private static String[] getStartAndEnd(Map<String, List<String>> graph) {
        String[] result = new String[2];
        Map<String, Integer> counter = new HashMap<>();
        for (String node : graph.keySet()) {
            counter.put(node, graph.get(node).size());
        }
        for (String node : graph.keySet()) {
            for (String node_ : graph.get(node)) {
                counter.put(node_, counter.get(node_) - 1);
            }
        }

        int min = 0;
        int max = 0;

        for (String node : counter.keySet()) {
            if (counter.get(node) <= min) {
                min = counter.get(node);
                result[1] = node;
            }
            if (counter.get(node) >= max) {
                max = counter.get(node);
                result[0] = node;
            }
        }

        return result;
    }

    private static boolean isEdgeInCycle(List<String> cycle, String node, String node_) {
        for (int i = 0; i < cycle.size() - 1; i++) {
            if (cycle.get(i).equals(node) && cycle.get(i + 1).equals(node_))
                return true;
        }
        return false;
    }

    private static String getNextNode(List<String> cycle, Map<String, List<String>> graph, String node) {
        for (String node_ : graph.get(node)) {
            if (!isEdgeInCycle(cycle, node, node_))
                return node_;
        }
        return "";
    }

    private static String getNewStart(List<String> cycle, Map<String, List<String>> graph) {
        for (String node : cycle) {
            String nextNode = getNextNode(cycle, graph, node);
            if (!nextNode.isEmpty())
                return node;
        }
        return "";
    }

    private static List<String> constructNewCycle(List<String> cycle, String newStart) {
        int position = 0;
        for (int i = 0; i < cycle.size(); i++) {
            if (cycle.get(i).equals(newStart))
                position = i;
        }

        List<String> newCycle = new ArrayList<>(cycle.size());
        for (int i = position; i < cycle.size(); i++) {
            newCycle.add(cycle.get(i));
        }
        for (int i = 1; i <= position; i++) {
            newCycle.add(cycle.get(i));
        }

        return newCycle;
    }

    private static List<String> getPairedReadsInOrder(List<String> pairedReads) {
        Map<String, List<String>> graph = createGraph(pairedReads);;
        String[] startAndEnd = getStartAndEnd(graph);
        String start = startAndEnd[0];
        String end = startAndEnd[1];
        graph.get(end).add(start);

        List<String> cycle = new ArrayList<>();
        cycle.add(graph.keySet().stream().findFirst().orElse(null));

        while (!getNewStart(cycle, graph).isEmpty()) {
            String newStart = getNewStart(cycle, graph);
            cycle = constructNewCycle(cycle, newStart);

            while (!getNextNode(cycle, graph, cycle.get(cycle.size() - 1)).isEmpty()) {
                cycle.add(getNextNode(cycle, graph, cycle.get(cycle.size() - 1)));
            }
        }

        List<String> inOrder = constructNewCycle(cycle, start);
        inOrder.remove(inOrder.size() - 1);
        return inOrder;
    }

    private static String mergeTogether(List<String> pairedReadsInOrder, int k, int d) {
        StringBuilder stringBuilder = new StringBuilder();
        String firstPattern = pairedReadsInOrder.get(0).split("\\|")[0];
        stringBuilder.append(firstPattern.substring(0, k - 2));

        for (String read : pairedReadsInOrder) {
            String first = read.split("\\|")[0];
            stringBuilder.append(first.charAt(k - 2));
        }

        int secondStartIdx = pairedReadsInOrder.size() - k - d;
        if (secondStartIdx < 0)
            secondStartIdx += pairedReadsInOrder.size();

        for (int i = secondStartIdx; i < pairedReadsInOrder.size(); i++) {
            String second = pairedReadsInOrder.get(i).split("\\|")[1];
            stringBuilder.append(second.charAt(k - 2));
        }

        return stringBuilder.toString();
    }

    public static void main(String[] args) {
        try (Scanner scan = new Scanner(Paths.get("dataset.txt"))) {
            int k = scan.nextInt();
            int d = scan.nextInt();
            scan.nextLine();
            List<String> pairedReads = new ArrayList<>();
            while (scan.hasNextLine()) {
                pairedReads.add(scan.next());
            }

            String text = mergeTogether(getPairedReadsInOrder(pairedReads), k, d);
            System.out.println(text);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}