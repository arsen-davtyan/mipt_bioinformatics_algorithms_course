import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.stream.Collectors;

public class Problem28 {

    private static void fillGraph(Map<Integer, Set<Integer>> graph, List<List<Integer>> genome) {
        for (List<Integer> block : genome) {
            for (int i = 0; i < block.size(); i++) {
                int nextElem = block.get((i + 1) % block.size());
                if (!graph.containsKey(block.get(i)))
                    graph.put(block.get(i), new HashSet<>());
                graph.get(block.get(i)).add(-nextElem);
                if (!graph.containsKey(-nextElem))
                    graph.put(-nextElem, new HashSet<>());
                graph.get(-nextElem).add(block.get(i));
            }
        }
    }

    private static Set<Integer> getNodes(Map<Integer, Set<Integer>> graph) {
        Set<Integer> nodes = new HashSet<>(graph.keySet());
        for (Set<Integer> value : graph.values())
            nodes.addAll(value);
        return nodes;
    }

    private static int countCycles(Map<Integer, Set<Integer>> graph) {
        int numCycles = 0;
        Set<Integer> nodes = getNodes(graph);
        while (!nodes.isEmpty()) {


            int node = nodes.stream().findFirst().orElse(Integer.MAX_VALUE);
            int nextNode = -Integer.MAX_VALUE;

            numCycles++;
            for (int node_ : graph.get(node)) {
                if (nodes.contains(node_)) {
                    nextNode = node_;
                    break;
                }
            }
            while (nodes.contains(nextNode)) {
                nodes.remove(nextNode);
                for (int node_ : graph.get(nextNode)) {
                    if (nodes.contains(node_)) {
                        nextNode = node_;
                        break;
                    }
                }
            }
        }

        return numCycles;
    }

    public static void main(String[] args) {
        try {
            Scanner scan = new Scanner(Paths.get("dataset.txt"));
            String line1 = scan.nextLine();
            String line2 = scan.nextLine();

            List<String> split1 = Arrays.stream(line1.split("[\\(\\)]")).filter(s -> !s.isEmpty()).collect(Collectors.toList());
            List<String> split2 = Arrays.stream(line2.split("[\\(\\)]")).filter(s -> !s.isEmpty()).collect(Collectors.toList());

            List<List<Integer>> genome1 = new ArrayList<>();
            List<List<Integer>> genome2 = new ArrayList<>();

            split1.forEach(block -> genome1.add(
                    Arrays.stream(block.split(" ")).mapToInt(Integer::parseInt).boxed().collect(Collectors.toList())
            ));
            split2.forEach(block -> genome2.add(
                    Arrays.stream(block.split(" ")).mapToInt(Integer::parseInt).boxed().collect(Collectors.toList())
            ));

            Map<Integer, Set<Integer>> graphPQ = new HashMap<>();
            fillGraph(graphPQ, genome1);
            fillGraph(graphPQ, genome2);

            Map<Integer, Set<Integer>> graphQQ = new HashMap<>();
            fillGraph(graphQQ, genome2);
            fillGraph(graphQQ, genome2);

            int distance = countCycles(graphQQ) - countCycles(graphPQ);
            System.out.println(distance);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}