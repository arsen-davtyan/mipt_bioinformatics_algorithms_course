import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Problem15 {

    private static String mergeTogether(List<String> pairedReads, int k, int d) {
        StringBuilder stringBuilder = new StringBuilder();
        String firstPattern = pairedReads.get(0).split("\\|")[0];
        stringBuilder.append(firstPattern.substring(0, k - 1));

        for (String read : pairedReads) {
            String first = read.split("\\|")[0];
            stringBuilder.append(first.charAt(k - 1));
        }

        int secondStartIdx = pairedReads.size() - k - d;
        if (secondStartIdx < 0)
            secondStartIdx += pairedReads.size();

        for (int i = secondStartIdx; i < pairedReads.size(); i++) {
            String second = pairedReads.get(i).split("\\|")[1];
            stringBuilder.append(second.charAt(k - 1));
        }

        return stringBuilder.toString();
    }

    public static void main(String[] args) {
        try (Scanner scan = new Scanner(Paths.get("dataset.txt"))) {
            int k = scan.nextInt();
            int d = scan.nextInt();
            scan.nextLine();
            List<String> pairedReads = new ArrayList<>();
            while (scan.hasNextLine()) {
                pairedReads.add(scan.next());
            }

            String text = mergeTogether(pairedReads, k, d);
            System.out.println(text);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}