import java.io.IOException;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.stream.IntStream;

public class Problem23 {
    private static final Map<Character, Integer> symbolIdx = new HashMap<>();
    private static final int[][] scoringMatrix = new int[20][20];

    private static final int sigma = 5;

    private static int getScore(char s1, char s2) {
        return scoringMatrix[symbolIdx.get(s1)][symbolIdx.get(s2)];
    }

    private static String getAlignment(String proteinString, String directionString, char direction) {
        StringBuilder alignment = new StringBuilder();
        int idx = 0;
        for (char dir : directionString.toCharArray()) {
            if (dir == direction) {
                alignment.append('-');
            } else {
                alignment.append(proteinString.charAt(idx));
                idx += 1;
            }
        }
        return alignment.toString();
    }

    public static void main(String[] args) {
        char[] symbols = {'A', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'K', 'L', 'M', 'N', 'P', 'Q', 'R', 'S', 'T', 'V', 'W', 'Y'};
        IntStream.range(0, 20).forEach(idx -> symbolIdx.put(symbols[idx], idx));

        try {
            Scanner scan = new Scanner(Paths.get("blosum62.txt"));
            for (int i = 0; i < 20; i++) {
                scoringMatrix[i] = Arrays.stream(scan.nextLine().split("\\s+")).mapToInt(Integer::parseInt).toArray();
            }
            scan.close();

            scan = new Scanner(Paths.get("dataset.txt"));
            String strI = scan.nextLine();
            String strJ = scan.nextLine();
            int n = strI.length();
            int m = strJ.length();

            int[][] path = new int[n + 1][m + 1];
            char[][] direction = new char[n + 1][m + 1];

            for (int i = 0; i < n + 1; i++) {
                for (int j = 0; j < m + 1; j++) {
                    if (i == 0 && j == 0)
                        continue;

                    int left = -Integer.MAX_VALUE;
                    int up = -Integer.MAX_VALUE;
                    int diag = -Integer.MAX_VALUE;

                    if (i >= 1 && j >= 1)
                        diag = path[i - 1][j - 1] + getScore(strI.charAt(i - 1), strJ.charAt(j - 1));
                    if (j >= 1)
                        left = path[i][j - 1] - sigma;
                    if (i >= 1)
                        up = path[i - 1][j] - sigma;

                    if (left >= up && left >= diag) {
                        path[i][j] = left;
                        direction[i][j] = 'l';
                    } else if (up >= left && up >= diag) {
                        path[i][j] = up;
                        direction[i][j] = 'u';
                    } else if (diag >= left && diag >= up) {
                        path[i][j] = diag;
                        direction[i][j] = 'd';
                    }
                }
            }

            StringBuilder directionStringBuilder = new StringBuilder();
            int i = n;
            int j = m;

            while (i != 0 || j != 0) {
                if (direction[i][j] == 'l') {
                    j--;
                    directionStringBuilder.append('r');
                } else if (direction[i][j] == 'u') {
                    i--;
                    directionStringBuilder.append('d');
                } else if (direction[i][j] == 'd') {
                    i--;
                    j--;
                    directionStringBuilder.append('D');
                }
            }

            String directionString = directionStringBuilder.reverse().toString();

            int score = path[n][m];
            String alignmentI = getAlignment(strI, directionString, 'r');
            String alignmentJ = getAlignment(strJ, directionString, 'd');

            System.out.println(score);
            System.out.println(alignmentI);
            System.out.println(alignmentJ);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}