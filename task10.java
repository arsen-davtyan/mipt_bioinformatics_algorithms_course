import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Scanner;

public class Problem10 {

    private static int getHammingDistance(String str1, String str2) {
        int hammingDistance = 0;
        for (int i = 0; i < str1.length(); i++) {
            if (str1.charAt(i) != str2.charAt(i))
                hammingDistance++;
        }
        return hammingDistance;
    }

    private static int distanceBetweenPatternAndStrings(String pattern, ArrayList<String> dna) {
        int totalDistance = 0;

        for (String text : dna) {
            int hammingDistance = Integer.MAX_VALUE;

            for (int i = 0; i <= text.length() - pattern.length(); i++) {
                String pattern_ = text.substring(i, i + pattern.length());
                int curDistance = getHammingDistance(pattern, pattern_);
                if (hammingDistance > curDistance)
                    hammingDistance = curDistance;
            }

            totalDistance += hammingDistance;
        }

        return totalDistance;
    }

    public static void main(String[] args) {
        try (Scanner scan = new Scanner(Paths.get("dataset.txt"))) {
            String pattern = scan.nextLine();
            ArrayList<String> dna = new ArrayList<>();
            while (scan.hasNext())
                dna.add(scan.next());

            System.out.println(distanceBetweenPatternAndStrings(pattern, dna));

        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
    }
}