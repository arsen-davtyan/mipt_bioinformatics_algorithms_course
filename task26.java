import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Problem26 {

    private static void reverse(List<Integer> sequence, int idx1, int idx2) {
        if (idx2 < idx1)
            return;

        List<Integer> res = new ArrayList<>(sequence.subList(0, idx1));
        List<Integer> reversed = sequence.subList(idx1, idx2 + 1);
        Collections.reverse(reversed);
        IntStream.range(0, idx2 - idx1 + 1).forEach(i -> reversed.set(i, -reversed.get(i)));
        res.addAll(reversed);
        res.addAll(sequence.subList(idx2 + 1, sequence.size()));

        sequence = res;
    }

    private static void printSequence(List<Integer> sequence) {
        System.out.print("(");
        for (int i = 0; i < sequence.size(); i++) {
            if (i == sequence.size() - 1) {
                if (sequence.get(i) > 0)
                    System.out.print("+");
                System.out.println(sequence.get(i) + ")");
            } else {
                if (sequence.get(i) > 0)
                    System.out.print("+");
                System.out.print(sequence.get(i) + " ");
            }
        }
    }

    private static void greedySorting(List<Integer> sequence) {
        for (int idx1 = 0; idx1 < sequence.size(); idx1++) {
            if (sequence.get(idx1) == idx1 + 1) {
                continue;
            }

            int idx2 = 0;
            if (sequence.contains(idx1  + 1)) {
                idx2 = sequence.indexOf(idx1 + 1);
            } else {
                idx2 = sequence.indexOf(-idx1 - 1);
            }

            reverse(sequence, idx1, idx2);
            printSequence(sequence);

            if (sequence.get(idx1) < 0) {
                reverse(sequence, idx1, idx1);
                printSequence(sequence);
            }

        }
    }

    public static void main(String[] args) {
        try {
            Scanner scan = new Scanner(Paths.get("dataset.txt"));
            List<Integer> permutation = Arrays.stream(scan.nextLine()
                    .replaceAll("\\(", "")
                    .replaceAll("\\)", "")
                    .split(" "))
                    .mapToInt(Integer::parseInt).boxed().collect(Collectors.toList());

            greedySorting(permutation);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}