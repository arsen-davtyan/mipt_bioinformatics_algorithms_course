import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class Problem16 {

    private static final Map<String, String> codonMap = new HashMap<>();

    private static String getReverseComplement(String pattern) {
        StringBuilder reverseComplementBuilder = new StringBuilder(pattern.length());
        for (int i = pattern.length() - 1; i >= 0; i--) {
            switch (pattern.charAt(i)) {
                case 'A':
                    reverseComplementBuilder.append('T');
                    break;
                case 'C':
                    reverseComplementBuilder.append('G');
                    break;
                case 'T':
                    reverseComplementBuilder.append('A');
                    break;
                case 'G':
                    reverseComplementBuilder.append('C');
            }
        }
        return reverseComplementBuilder.toString();
    }

    private static String translate(String pattern) {
        StringBuilder translated = new StringBuilder();
        String transcripted = pattern.replace('T', 'U');
        for (int i = 0; i < transcripted.length() / 3; i++) {
            if (codonMap.containsKey(transcripted.substring(3 * i, 3 * i + 3))) {
                translated.append(codonMap.get(transcripted.substring(3 * i, 3 * i + 3)));
            }
        }
        return translated.toString();
    }

    private static List<String> getEncodingPatterns(String text, String peptide) {
        List<String> patterns = new ArrayList<>();
        int patternLen = peptide.length() * 3;
        for (int i = 0; i < text.length() - patternLen; i++) {
            if (translate(text.substring(i, i + patternLen)).equals(peptide)
                    || translate(getReverseComplement(text.substring(i, i + patternLen))).equals(peptide)) {
                patterns.add(text.substring(i, i + patternLen));
            }
        }
        return patterns;
    }

    public static void main(String[] args) {
        try {
            Scanner scan = new Scanner(Paths.get("dataset.txt"));
            String text = scan.nextLine();
            String peptide = scan.nextLine();
            scan.close();
            scan = new Scanner(Paths.get("codons.txt"));
            while (scan.hasNextLine()) {
                String[] tokens = scan.nextLine().split(" ");
                codonMap.put(tokens[0], tokens[1]);
            }

            List<String> encodingPatterns = getEncodingPatterns(text, peptide);
            for (String pattern : encodingPatterns)
                System.out.println(pattern);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
}