import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Scanner;

public class Problem4 {
    private static char[] alphabet = {'A', 'C', 'T', 'G'};
    private static HashMap<String, Integer> patternFreqs = new HashMap<>();

    private static int countMismatches(String pattern1, String pattern2) {
        if (pattern1.length() != pattern2.length())
            return -1;

        int count = 0;
        for (int i = 0; i < pattern1.length(); i++) {
            if (pattern1.charAt(i) != pattern2.charAt(i))
                count++;
        }
        return count;
    }

    private static int countApproxOccurrences(String pattern, String text, int d) {
        int count = 0;
        for (int i = 0; i <= text.length() - pattern.length(); i++) {
            if (countMismatches(pattern, text.substring(i, i + pattern.length())) <= d)
                count += 1;
        }

        return count;
    }

    private static void iteratePatterns(char[] build, int pos, String text, int k, int d) {
        if (pos == k) {
            String pattern = new String(build);
            int freq = countApproxOccurrences(pattern, text, d);
            patternFreqs.put(pattern, freq);
            return;
        }

        for (int i = 0; i < 4; i++) {
            build[pos] = alphabet[i];
            iteratePatterns(build, pos + 1, text, k, d);
        }
    }

    private static ArrayList<String> getMostFrequentPatterns(String text, int k, int d) {
        ArrayList<String> mostFrequentPatterns = new ArrayList<>();
        iteratePatterns(new char[k], 0, text, k, d);
        int maxFreq = Collections.max(patternFreqs.values());
        for (String pattern : patternFreqs.keySet()) {
            if (patternFreqs.get(pattern) == maxFreq)
                mostFrequentPatterns.add(pattern);
        }
        return mostFrequentPatterns;
    }

    public static void main(String[] args) {
        try (Scanner scan = new Scanner(Paths.get("dataset.txt"))) {
            String text = scan.nextLine();
            int k = scan.nextInt();
            int d = scan.nextInt();
            ArrayList<String> mostFrequentPatterns = getMostFrequentPatterns(text, k, d);
            for (String pattern : mostFrequentPatterns)
                System.out.print(pattern + " ");
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
    }
}