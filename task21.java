import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class Problem21 {
    public static void main(String[] args) {
        try (Scanner scan = new Scanner(Paths.get("dataset.txt"))) {
            int money = Integer.parseInt(scan.nextLine());
            List<Integer> coins = Arrays.stream(scan.nextLine().split(",")).mapToInt(Integer::parseInt).boxed().collect(Collectors.toList());

            List<Integer> numCoins = new ArrayList<>(money + 1);
            for (int i = 0; i < money + 1; i++) {
                numCoins.add(money);
            }
            numCoins.set(0, 0);

            for (int i = 1; i < money + 1; i++) {
                for (int coin : coins) {
                    if (i - coin >= 0) {
                        numCoins.set(i, Collections.min(Arrays.asList(numCoins.get(i), numCoins.get(i - coin) + 1)));
                    }
                }
            }

            System.out.println(numCoins.get(money));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}