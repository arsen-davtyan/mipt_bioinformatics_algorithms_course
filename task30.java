import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class Problem30 {

    private static List<Integer> cycleToChromosome(List<Integer> cycle) {
        List<Integer> chromosome = new ArrayList<>();
        for (int i = 0; i < cycle.size() / 2; i++) {
            if (cycle.get(2 * i) < cycle.get(2 * i + 1)) {
                chromosome.add(cycle.get(2 * i + 1) / 2);
            } else {
                chromosome.add(-(cycle.get(2 * i) / 2));
            }
        }
        return chromosome;
    }

    private static List<Integer> chromosomeToCycle(List<Integer> chromosome) {
        List<Integer> cycle = new ArrayList<>();
        for (int elem : chromosome) {
            if (elem > 0) {
                cycle.add(2 * elem - 1);
                cycle.add(2 * elem);
            } else {
                cycle.add(-2 * elem);
                cycle.add(-2 * elem - 1);
            }
        }
        return cycle;
    }

    private static List<List<Integer>> coloredEdges(List<List<Integer>> genome) {
        List<List<Integer>> edges = new ArrayList<>();
        for (List<Integer> chromosome : genome) {
            List<Integer> nodes = chromosomeToCycle(chromosome);

            for (int i = 1; i < chromosome.size() + 1; i++) {
                List<Integer> edge = new ArrayList<>();
                edge.add(nodes.get((2 * i - 1) % nodes.size()));
                edge.add(nodes.get((2 * i) % nodes.size()));
                edges.add(edge);
            }
        }

        return edges;
    }

    private static List<List<Integer>> breakOnGenomeGraph(List<List<Integer>> graph, int i, int i_, int j, int j_) {
        List<Integer> edge = Arrays.asList(i, i_);
        if (graph.contains(edge)) {
            graph.remove(edge);
        } else {
            graph.remove(Arrays.asList(i_, i));
        }

        edge = Arrays.asList(j, j_);
        if (graph.contains(edge)) {
            graph.remove(edge);
        } else {
            graph.remove(Arrays.asList(j, j_));
        }

        graph.add(Arrays.asList(i, j));
        graph.add(Arrays.asList(i_, j_));

        return graph;
    }

    private static List<List<Integer>> graphToGenome(List<List<Integer>> graph) {
        List<List<Integer>> genome = new ArrayList<>();

        while (!graph.isEmpty()) {
            int num = graph.get(0).get(0);
            List<Integer> nodes = new ArrayList<>();
            while (true) {
                boolean isFound = false;

                for (List<Integer> elem : graph) {
                    if (elem.contains(num)) {
                        nodes.add(num);
                        if (elem.get(0) != num) {
                            nodes.add(elem.get(0));
                        } else {
                            nodes.add(elem.get(1));
                        }
                        graph.remove(elem);
                        isFound = true;
                        break;
                    }
                }

                if (!isFound) {
                    List<Integer> nodes_ = new ArrayList<>();
                    nodes_.add(nodes.get(nodes.size() - 1));
                    nodes_.addAll(nodes.subList(0, nodes.size() - 1));
                    List<Integer> chromosome = cycleToChromosome(nodes_);
                    genome.add(chromosome);
                    break;
                }

                if (nodes.get(nodes.size() - 1) % 2 == 0) {
                    num = nodes.get(nodes.size() - 1) - 1;
                } else {
                    num = nodes.get(nodes.size() - 1) + 1;
                }
            }
        }

        return genome;
    }


    private static List<List<Integer>> breakOnGenome(List<Integer> genome, int i, int i_, int j, int j_) {
        List<List<Integer>> graph = new ArrayList<>();
        graph.add(genome);

        graph = coloredEdges(graph);
        graph = breakOnGenomeGraph(graph, i, i_, j, j_);
        List<List<Integer>> resultGenome = graphToGenome(graph);

        return resultGenome;
    }


    public static void main(String[] args) {
        try {
            Scanner scan = new Scanner(Paths.get("dataset.txt"));

            String line = scan.nextLine();
            List<String> split = Arrays.stream(line.split("[\\(\\)\\s+]")).filter(s -> !s.isEmpty()).collect(Collectors.toList());
            List<Integer> genome = split.stream().mapToInt(Integer::parseInt).boxed().collect(Collectors.toList());
            line = scan.nextLine();
            String[] indices = line.split(", ");
            int i = Integer.parseInt(indices[0]);
            int i_ = Integer.parseInt(indices[1]);
            int j = Integer.parseInt(indices[2]);
            int j_ = Integer.parseInt(indices[3]);

            List<List<Integer>> resultGenome = breakOnGenome(genome, i, i_, j, j_);
            for (List<Integer> chromosome : resultGenome) {
                System.out.print("(");
                for (int k = 0; k < chromosome.size() - 1; k++) {
                    if (chromosome.get(k) > 0) {
                        System.out.print("+");
                    }
                    System.out.print(chromosome.get(k));
                    System.out.print(" ");
                }
                if (chromosome.get(chromosome.size() - 1) > 0) {
                    System.out.print("+");
                }
                System.out.print(chromosome.get(chromosome.size() - 1));
                System.out.print(")");
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}