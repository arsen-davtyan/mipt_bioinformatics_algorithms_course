import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.stream.Collectors;

public class Problem14 {

    private static Map<String, List<String>> createGraph(List<String> patterns) {
        Map<String, List<String>> graph = new HashMap<>();
        for (String pattern : patterns) {
            String prefix = pattern.substring(0, pattern.length() - 1);
            String suffix = pattern.substring(1);
            if (!graph.containsKey(prefix))
                graph.put(prefix, new ArrayList<>());
            if (!graph.containsKey(suffix))
                graph.put(suffix, new ArrayList<>());
        }
        for (String pattern : patterns) {
            String prefix = pattern.substring(0, pattern.length() - 1);
            String suffix = pattern.substring(1);
            graph.get(prefix).add(suffix);
        }
        return graph;
    }

    private static List<String> getNonBranchingNodes(Map<String, List<String>> graph) {
        Map<String, Integer> countOut = new HashMap<>();
        for (String node : graph.keySet()) {
            countOut.put(node, graph.get(node).size());
        }

        Map<String, Integer> countIn = new HashMap<>();
        for (String node : graph.keySet()) {
            for (String node_ : graph.get(node)) {
                if (countIn.containsKey(node_))
                    countIn.put(node_, countIn.get(node_) + 1);
                else
                    countIn.put(node_, 1);
            }
        }

        return graph.keySet().stream().filter(node ->
                countOut.containsKey(node) && countOut.get(node) == 1 &&
                countIn.containsKey(node) && countIn.get(node) == 1).collect(Collectors.toList());
    }

    private static List<List<String>> initContigs(Map<String, List<String>> graph) {
        List<List<String>> contigs = new ArrayList<>();
        for (String node : graph.keySet()) {
            for (String node_ : graph.get(node)) {
                contigs.add(new ArrayList<>(Arrays.asList(node, node_)));
            }
        }

        return contigs;
    }

    private static String mergeTogether(List<String> patternsInOrder) {
        int patternSize = patternsInOrder.get(0).length();

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(patternsInOrder.get(0).substring(0,  patternSize - 1));
        for (String pattern : patternsInOrder) {
            stringBuilder.append(pattern.charAt(patternSize - 1));
        }

        return stringBuilder.toString();
    }

    private static List<String> getAllContigs(Map<String, List<String>> graph) {
        List<List <String>> contigs = initContigs(graph);
        List<String> nonBranching = getNonBranchingNodes(graph);

        List<String> left = new ArrayList<>();
        List<String> right = new ArrayList<>();
        for (String node: nonBranching) {
            for (List<String> contig : contigs) {
                if (contig.get(0).equals(node)) {
                    right = contig;
                }
                if (contig.get(contig.size() - 1).equals(node)) {
                    left = contig;
                }
            }

            for (int i = 1; i < right.size(); i++) {
                left.add(right.get(i));
            }
            contigs.remove(right);
        }

        return contigs.stream().map(contig -> mergeTogether(contig)).collect(Collectors.toList());
    }

    public static void main(String[] args) {
        try (Scanner scan = new Scanner(Paths.get("dataset.txt"))) {
            List<String> patterns = new ArrayList<>();
            while (scan.hasNextLine()) {
                patterns.add(scan.nextLine());
            }

            List<String> contigs = getAllContigs(createGraph(patterns));
            contigs.stream().forEach(c -> System.out.print(c + " "));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}