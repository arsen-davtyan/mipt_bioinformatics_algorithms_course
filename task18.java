import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class Problem18 {
    private static List<Long> getMassesInSpectrum(List<Long> spectrum) {
        List<Long> masses = Arrays.asList(57L, 71L, 87L, 97L, 99L, 101L, 103L, 113L, 114L, 115L, 128L, 129L, 131L, 137L,
                147L, 156L, 163L, 186L);
        return masses.stream().filter(spectrum::contains).collect(Collectors.toList());
    }

    private static List<Peptide> expand(List<Peptide> peptides, List<Long> masses) {
        List<Peptide> newPeptides = new ArrayList<>();
        for (Peptide p : peptides) {
            for (Long m : masses) {
                Peptide p_ = p.copy();
                p_.add(m);
                newPeptides.add(p_);
            }
        }
        return newPeptides;
    }

    private static boolean areSpectrumsEqual(List<Long> sp1, List<Long> sp2) {
        for (int i = 0; i < sp1.size(); i++) {
            if (!sp1.get(i).equals(sp2.get(i))) {
                return false;
            }
        }
        return true;
    }

    private static void cyclopeptideSequencing(List<Long> spectrum) {
        List<Long> masses = getMassesInSpectrum(spectrum);
        List<Peptide> peptides = Arrays.asList(new Peptide());
        while (peptides.size() > 0) {
            peptides = expand(peptides, masses);
            List<Peptide> newPeptides = new ArrayList<>(peptides);
            for (Peptide peptide : peptides) {
                if (peptide.getMass() == Collections.max(spectrum)) {
                    if (areSpectrumsEqual(peptide.getSpectrum(), spectrum)) {
                        System.out.print(peptide + " ");
                    }
                    newPeptides.remove(peptide);
                }
                else if (!spectrum.contains(peptide.getMass())) {
                    newPeptides.remove(peptide);
                }
            }
            peptides = newPeptides;
        }
    }

    public static void main(String[] args) {
        try (Scanner scan = new Scanner(Paths.get("dataset.txt"))) {
            List<Long> spectrum = new ArrayList<>();
            while (scan.hasNextLong()) {
                spectrum.add(scan.nextLong());
            }
            Collections.sort(spectrum);
            cyclopeptideSequencing(spectrum);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}

class Peptide {
    private List<Long> masses = new ArrayList<>();

    List<Long> getSpectrum() {
        ArrayList<Long> spectrum = new ArrayList<>(Arrays.asList(0L));
        List<Long> mses = new ArrayList<>(this.masses);
        mses.remove(mses.size() - 1);
        int n = this.masses.size();

        for (int k = 1; k < n; k++) {
            int idx = (k - 2) % n;
            if (idx < 0)
                idx += n;
            mses.add(this.masses.get(idx));
            for (int i = 0; i < mses.size() - k + 1; i++) {
                long sum = 0;
                for (int j = i; j < i + k; j++)
                    sum += mses.get(j);
                spectrum.add(sum);
            }
        }
        spectrum.add(getMass());
        Collections.sort(spectrum);
        return spectrum;
    }

    long getMass() {
        return masses.stream().reduce(0L, Long::sum);
    }

    void add(long newMass) {
        this.masses.add(newMass);
    }

    Peptide copy() {
        Peptide copyPeptide = new Peptide();
        copyPeptide.masses = new ArrayList<>(this.masses);
        return copyPeptide;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (long mass : this.masses) {
            sb.append(Long.toString(mass));
            sb.append("-");
        }
        sb.deleteCharAt(sb.length() - 1);
        return sb.toString();
    }
}