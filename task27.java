import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class Problem27 {
    public static void main(String[] args) {
        try {
            Scanner scan = new Scanner(Paths.get("dataset.txt"));
            List<Integer> permutation = Arrays.stream(scan.nextLine()
                    .replaceAll("\\(", "")
                    .replaceAll("\\)", "")
                    .split(" "))
                    .mapToInt(Integer::parseInt).boxed().collect(Collectors.toList());

            List<Integer> sequence = new ArrayList<>();
            sequence.add(0);
            sequence.addAll(permutation);
            sequence.add(permutation.size() + 1);

            int numBreakpoints = 0;
            for (int i = 0; i < sequence.size(); i++) {
                if (i == 0)
                    continue;

                if (sequence.get(i) - sequence.get(i - 1) != 1)
                    numBreakpoints++;
            }

            System.out.println(numBreakpoints);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}