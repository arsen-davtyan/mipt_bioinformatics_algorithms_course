import java.nio.file.Paths;
import java.util.*;

public class Problem7 {

    private static final char[] nucleotides = {'A', 'C', 'G', 'T'};

    private static int getNucleotideIdx(char c) {
        for (int i = 0; i < nucleotides.length; i++) {
            if (nucleotides[i] == c)
                return i;
        }
        return -1;
    }

    private static char[][] copyChar2dMatrix(char[][] original) {
        char[][] copy = new char[original.length][];
        for (int i = 0; i < original.length; i++) {
            copy[i] = Arrays.copyOf(original[i], original[i].length);
        }
        return copy;
    }

    private static char[][] initMotifMatrix(String[] dna, int k) {
        char[][] motifMatrix = new char[dna.length][k];

        for (int i = 0; i < dna.length; i++) {
            motifMatrix[i] = dna[i].substring(0, k).toCharArray();
        }

        return motifMatrix;
    }

    private static double[][] formProfile(char[][] motifMatrix, int k, int idx) {
        double[][] profile = new double[k][nucleotides.length];
        for (int j = 0; j < k; j++) {
            for (int i = 0; i < idx; i++) {
                profile[j][getNucleotideIdx(motifMatrix[i][j])] += 1;
            }
        }

        for (int i = 0; i < k; i++) {
            for (int j = 0; j < nucleotides.length; j++) {
                profile[i][j] = (profile[i][j] + 1) / (idx + nucleotides.length);
            }
        }

        return profile;
    }

    private static String getMostProbablePattern(double[][] profile, int k, String str) {
        String mostProbablePattern = str.substring(0, k);
        double maxProba = 0;
        for (int i = 0; i <= str.length() - k; i++) {
            String pattern = str.substring(i, i + k);
            double proba = 1;
            for (int j = 0; j < pattern.length(); j++) {
                proba *= profile[j][getNucleotideIdx(pattern.charAt(j))];
            }
            if (proba > maxProba) {
                maxProba = proba;
                mostProbablePattern = pattern;
            }
        }
        return mostProbablePattern;
    }

    private static int scoreMotifs(char[][] motifMatrix, int k, int t) {
        int totalScore = 0;

        HashMap<Character, Integer> symbolFreq = new HashMap<>();
        for (int j = 0; j < k; j++) {
            for (int i = 0; i < t; i++) {
                if (symbolFreq.containsKey(motifMatrix[i][j])) {
                    symbolFreq.put(motifMatrix[i][j], symbolFreq.get(motifMatrix[i][j]) + 1);
                } else {
                    symbolFreq.put(motifMatrix[i][j], 1);
                }
            }
            totalScore += symbolFreq.values().stream().reduce(0, Integer::sum) - Collections.max(symbolFreq.values());
            symbolFreq.clear();
        }

        return totalScore;
    }

    private static char[][] greedyMotifSearch(String[] dna, int k, int t) {
        char[][] motifMatrix = initMotifMatrix(dna, k);
        char[][] bestMotifs = copyChar2dMatrix(motifMatrix);

        for (int i = 0; i <= dna[0].length() - k; i++) {
            String motif = dna[0].substring(i, i + k);

            motifMatrix[0] = motif.toCharArray();

            for (int j = 1; j < t; j++) {
                double[][] profile = formProfile(motifMatrix, k, j);
                String mostProbablePattern = getMostProbablePattern(profile, k, dna[j]);

                motifMatrix[j] = mostProbablePattern.toCharArray();

            }

            if (scoreMotifs(motifMatrix, k, t) < scoreMotifs(bestMotifs, k, t))
                bestMotifs = copyChar2dMatrix(motifMatrix);
        }

        return bestMotifs;
    }

    public static void main(String[] args) {
        try (Scanner scan = new Scanner(Paths.get("dataset.txt"))) {
            int k = scan.nextInt();
            int t = scan.nextInt();
            scan.nextLine();
            String[] dna = new String[t];
            for (int i = 0; i < t; i++)
                dna[i] = scan.nextLine();

            char[][] bestMotifs = greedyMotifSearch(dna, k, t);
            for (char[] motif : bestMotifs)
                System.out.println(motif);


        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
    }
}