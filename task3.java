import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Scanner;

public class Problem3 {

    private static int countMismatches(String pattern1, String pattern2) {
        if (pattern1.length() != pattern2.length())
            return -1;

        int count = 0;
        for (int i = 0; i < pattern1.length(); i++) {
            if (pattern1.charAt(i) != pattern2.charAt(i))
                count++;
        }
        return count;
    }

    private static ArrayList<Integer> getApproxStartPositions(String pattern, String genome, int d) {
        ArrayList<Integer> startPositions = new ArrayList<>();
        for (int i = 0; i <= genome.length() - pattern.length(); i++) {
            if (countMismatches(pattern, genome.substring(i, i + pattern.length())) <= d)
                startPositions.add(i);
        }

        return startPositions;
    }

    public static void main(String[] args) {
        try (Scanner scan = new Scanner(Paths.get("dataset.txt"))) {
            String pattern = scan.nextLine();
            String genome = scan.nextLine();
            int d = scan.nextInt();

            ArrayList<Integer> startPositions =  getApproxStartPositions(pattern, genome, d);
            for (int i : startPositions) {
                System.out.print(i + " ");
            }

        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
    }
}