import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collections;
import java.util.Scanner;

public class Problem2 {

    private static Integer[] getSkews(String genome) {
        Integer[] allSkews = new Integer[genome.length() + 1];
        int currSkew = 0;
        allSkews[0] = currSkew;
        for (int i = 0; i < genome.length(); i++) {
            if (genome.charAt(i) == 'G')
                currSkew += 1;
            else if (genome.charAt(i) == 'C')
                currSkew -= 1;

            allSkews[i + 1] = currSkew;
        }

        return allSkews;
    };

    public static void main(String[] args) {
        try (Scanner scan = new Scanner(Paths.get("dataset.txt"))) {
            String genome = scan.nextLine();
            Integer[] allSkews = getSkews(genome);
            int minSkew = Collections.min(Arrays.asList(allSkews));
            for (int i = 0; i < allSkews.length; i++) {
                if (allSkews[i] == minSkew) {
                    System.out.print(i + " ");
                }
            }

        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
    }
}